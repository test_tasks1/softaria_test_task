package mainpackage;

import java.util.Scanner;

public class InputHandler {

    private Scanner inputScanner;
    InputHandler(){
        inputScanner = new Scanner(System.in);
    }

    public String getEmail(String ofWho){
        System.out.println("Enter " + ofWho + " email address:");
        return inputScanner.nextLine();
    }

    public String getFilename(String ofWhat){
        System.out.println("Enter filename " + ofWhat);
        return inputScanner.nextLine();
    }

    public String getPassword(){
        System.out.println("Enter your mail password");
        return inputScanner.nextLine();
    }

}
