package mainpackage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;

public class Main {

    public static void main(String[] args) {
        InputHandler ih = new InputHandler();
        String yesterdayFilename;
        String todayFilename;
        Parser yParser;
        Parser tParser;
        HashMap<String, ArrayList<String>> yesterdayTable = new HashMap<>();
        HashMap<String, ArrayList<String>> todayTable = new HashMap<>();
        yesterdayFilename = ih.getFilename("of first file with table");
        try {
            yParser = new Parser(yesterdayTable, new FileInputStream(new File(yesterdayFilename)));
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            return;
        }
        todayFilename = ih.getFilename("of second file with table");
        try {
            tParser = new Parser(todayTable, new FileInputStream(new File(todayFilename)));
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            return;
        }
        try {
            yParser.parse();
            tParser.parse();
        } catch (MalformedURLException e) {
            System.out.println("Wrong file format - incorrect URL");
        }
        Comparator comparator = new Comparator(yParser.getOutputTable(), tParser.getOutputTable());
        comparator.compareTables();

        StringBuilder sb = new StringBuilder();
        sb.append("Здравствуйте, дорогая и.о. секретаря.\n")
                .append("За последние сутки во вверенных Вам сайтах произошли следующие изменения:\n")
                .append("1) Исчезли следующие страницы:\n");
        comparator.getRemovedURL().forEach(str -> sb.append(str).append(" "));
        sb.append("\n2) Появились следующие страницы:\n");
        comparator.getAddedURL().forEach(str -> sb.append(str).append(" "));
        sb.append("\n3) Изменились следующие страницы: \n");
        comparator.getChangedURL().forEach(str -> sb.append(str).append(" "));
        sb.append("\nС уважением,\n")
                .append("автоматизированная система мониторинга.");
        System.out.println("Warning! Mail sender requires your mail to be gmail.");
        MailSender sender = new MailSender(ih.getEmail("sender"), ih.getPassword());
        try {
            sender.send("Test", sb.toString(), ih.getEmail("recipient"));
        } catch (Exception e) {
            System.out.println("Fail with an exception: \"" + e.getMessage() + "\"");
        }
    }
}
