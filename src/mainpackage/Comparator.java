package mainpackage;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class Comparator {

    private HashMap<String, ArrayList<String>> yesterdayTable;
    private HashMap<String, ArrayList<String>> todayTable;
    private ArrayList<String> addedURL;
    private ArrayList<String> removedURL;
    private ArrayList<String> changedURL;

    Comparator(HashMap<String, ArrayList<String>> yesterdayTable,
               HashMap<String, ArrayList<String>> todayTable){
        this.yesterdayTable = yesterdayTable;
        this.todayTable = todayTable;
        addedURL = new ArrayList<>();
        removedURL = new ArrayList<>();
        changedURL = new ArrayList<>();
    }

    public void compareTables(){
        for (String url: yesterdayTable.keySet()) {
            if (!todayTable.containsKey(url)) {
                removedURL.add(url);
            } else {
                if (!yesterdayTable.get(url).equals(todayTable.get(url))) {
                    changedURL.add(url);
                } else {
                }
                todayTable.remove(url);
            }
        }
        addedURL.addAll(todayTable.keySet());
    }

    public ArrayList<String> getAddedURL() {
        return addedURL;
    }

    public ArrayList<String> getRemovedURL() {
        return removedURL;
    }

    public ArrayList<String> getChangedURL() {
        return changedURL;
    }
}
