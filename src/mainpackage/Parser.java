package mainpackage;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Parser {
    private HashMap<String, ArrayList<String>> outputTable;
    private InputStream inputStream;

    Parser() {
        inputStream = System.in;
        outputTable = new HashMap<>();
    }

    Parser(HashMap<String, ArrayList<String>> outputTable, InputStream inputStream) {
        this.outputTable = outputTable;
        this.inputStream = inputStream;
    }

    public void parse() throws MalformedURLException {
        Scanner sc = new Scanner(inputStream);
        while (sc.hasNextLine()) {
            ArrayList<String> htmlText = new ArrayList<>();
            String nextLine = sc.nextLine();
            String urlString = nextLine;
            new URL(nextLine); //url validity check - will throw exception if nextLine is not URL
            do {
                nextLine = sc.nextLine();
                htmlText.add(nextLine);
            } while (!nextLine.equals("</html>"));
            outputTable.put(urlString, htmlText);
        }
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public HashMap<String, ArrayList<String>> getOutputTable() {
        return outputTable;
    }
}
